﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour 
{

    private Animator anim;

    public bool jump;
    public bool doubleJump;
    public bool grounded;
    public bool attack;
    public bool sprint;
    public bool dash;
    public bool climbing;
    public bool right;
    private bool dead;
    public bool crouch;

    public float horizontal;
    public float vertical;

	public void SetAnimationController(Animator _controller)
    {
        anim = _controller;
    }
	
	// Update is called once per frame
	void Update () 
	{
        //bools
        anim.SetBool("jump", jump);
        anim.SetBool("doubleJump", doubleJump);
        anim.SetBool("grounded", grounded);
        anim.SetBool("attack", attack);
        anim.SetBool("sprint", sprint);
        anim.SetBool("dash", dash);
        anim.SetBool("climbing", climbing);
        anim.SetBool("right", right);
        anim.SetBool("dead", dead);
        anim.SetBool("crouch", crouch);
	

        //floats
        anim.SetFloat("horizontal", horizontal);
        anim.SetFloat("vertical", vertical);


    }

    public void SetMeleeNum(int _ind)
    {
        anim.SetInteger("meleeNum", _ind);
    }

    public void SetDead(bool _isDead)
    {
        if (_isDead)
        {
            anim.SetInteger("deathNum", Random.Range(0, 3));
            dead = true;
        }
        else
        {
            anim.Play("Idle", 0);
        }

    }

	public void SetShootTrigger()
	{
		anim.SetTrigger ("shoot");
	}
}
