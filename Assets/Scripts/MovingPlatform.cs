﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

    [SerializeField]
    private float travelTime;
    [SerializeField]
    private List<Transform> points = new List<Transform>(); //using list so we can reverse sort for ping pong
    [SerializeField]
    private bool pingPong;

    private Vector3 curPos;
    private Vector3 nextPos;
    private int curInd = 0;

    private float lerpTimer;

	// Use this for initialization
	void Start ()
    {

        curPos = points[curInd].position;
        nextPos = points[curInd + 1].position;

    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {       
        lerpTimer += Time.deltaTime;
        if (lerpTimer > travelTime)
        {
            lerpTimer = travelTime;
        }
        float perc = lerpTimer / travelTime;

        transform.position = Vector3.Lerp(curPos, nextPos, perc);

        if (perc >= 1)
        {
            NextWaypoint();
            lerpTimer = 0;
        }      

	}

    void NextWaypoint()
    {
        curPos = transform.position;
        curInd++;
        if (curInd < points.Count - 1)
        {
            nextPos = points[curInd + 1].position;
        }
        else
        {
            if (pingPong)
            {
                points.Reverse();
                curInd = 0;
            }
            else
            {
                curInd = -1;
            }      
            nextPos = points[curInd + 1].position;
        }
   
    }
        
}
