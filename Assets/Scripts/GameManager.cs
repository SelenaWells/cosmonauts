﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    [SerializeField]
    private PlayerUI playerUI;
    [SerializeField]
    private PlayerData playerData;
    [SerializeField]
    private SpawnManager spawnManager;
    [SerializeField]
    private LevelManager levelManager;
    [SerializeField]
    private float killHeight = -20;
    [SerializeField]
    private MenuManager menuManager;
    [SerializeField]
    private GameObject musicManagerPrefab;
    private MusicManager musicManager;

	// Use this for initialization
	void Awake ()
    {
        instance = this;		
	}

    void Start()
    {
        BeginMusic();
    }
	
    void BeginMusic()
    {
        //find music manager
        var music = GameObject.Find("MusicManager");

        if (!music)//instantiate manager if not found
            musicManager = Instantiate(musicManagerPrefab).GetComponent<MusicManager>();
        else
            musicManager = music.GetComponent<MusicManager>();

        //play music
        musicManager.PlayBackgroundMusic();
    }

    public void LevelWin(int _nextScene, float _endTime, bool _freezeGame)
    {
        StartCoroutine(StartLevelWin(_nextScene, _endTime, _freezeGame));
    }

    IEnumerator StartLevelWin(int _nextScene, float _endTime, bool _freezeGame)
    {
        //do UI effects

        //freeze game
        float curScale = Time.timeScale;
        if (_freezeGame)
        {
            Time.timeScale = 0;
        }
            
        //play win music
        musicManager.PlayGameOverWinMusic();

        //wait to load next level
        yield return new WaitForSecondsRealtime(_endTime);

        Time.timeScale = curScale;

        //load level
        levelManager.LoadLevel(_nextScene);

    }

    public void LevelLose()
    {

    }

    public PlayerData GetPlayerData()
    {
        return playerData;
    }

	public PlayerUI GetPlayerUI()
    {
        return playerUI;
    }

    public SpawnManager GetSpawnManager()
    {
        return spawnManager;
    }

    public LevelManager GetLevelManager()
    {
        return levelManager;
    }

    public MenuManager GetMenuManager()
    {
        return menuManager;
    }

    public float GetKillHeight()
    {
        return killHeight;
    }

}
