﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{

    private Transform player;
    [SerializeField]
    private float xOffset;
    [SerializeField]
    private float yOffset;
    [SerializeField]
    private float Sensitivity = 10;

    void FixedUpdate()
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
            return;
        }

        Vector3 camPos = new Vector3(player.position.x + xOffset, 
            player.position.y + yOffset, transform.position.z);

        transform.position = Vector3.Lerp(transform.position, camPos, Time.deltaTime * Sensitivity);

    }
	
}
