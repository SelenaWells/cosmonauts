﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour 
{

    [SerializeField]
    private Transform[] checkPoints;
    [SerializeField]
    private bool progressOnly = true;
    [SerializeField]
    private bool resetLevelOnDeath = false;
    [SerializeField]
    private bool saveProgressToDisc = true;
    [SerializeField]
    private bool resetProgressOnQuit = true;
    [SerializeField]
    private GameObject playerSpawn;
    [SerializeField]
    private float respawnTime = 3;
    
    private int curCheckPoint;
    private Transform player;

    private LevelManager lm;
    private PlayerData pd;

    // Use this for initialization
    void Start () 
	{
        lm = GameManager.instance.GetLevelManager();
        pd = GameManager.instance.GetPlayerData();

        SetCheckPointIndexes();
        SpawnPlayer();
	}
	
    void SetCheckPointIndexes()
    {
        for (int i = 0; i < checkPoints.Length; i++)
        {
            checkPoints[i].GetComponent<CheckPoint>().SetCheckPointInd(i);
        }
    }

	public void SpawnPlayer()
    {
        string savedName = lm.GetCurSavedLevelName();
        if (savedName == lm.GetCurLevelName())
        {
            curCheckPoint = lm.GetCurSavedCheckPoint();
            Debug.Log("Saved level name matches current level...spawning player at checkpoint " + curCheckPoint);
        }
        else
        {
            Debug.Log("Saved level name does not match current level name...Resetting checkpoint to 0");
            curCheckPoint = 0;
        }
        
        //store spawn in transform var for respawn use
        player = Instantiate(playerSpawn, checkPoints[curCheckPoint].position, 
            checkPoints[curCheckPoint].rotation).transform;
    }

    public IEnumerator RespawnPlayer()
    {
        yield return new WaitForSeconds(respawnTime);

        if (pd.GetCurLives() > 0)
        {
            if (resetLevelOnDeath)
                //reset level at checkpoint 
                ResetLevel(curCheckPoint);
            else
                ResetPlayerPosition();
        }
        else
            //reset entire level at first checkpoint if no lives
            ResetLevel(0);

       
    }

    void ResetPlayerPosition()
    {
        Debug.Log("resetting player position to " + checkPoints[curCheckPoint].position);
        player.position = checkPoints[curCheckPoint].position;

        //reset player health
        Player health = player.GetComponent<Player>();
        health.SetHealthDefaults();
    }

    void ResetLevel(int _checkPoint)
    {
        lm.SaveCurCheckPoint(_checkPoint);
        lm.SaveCurLevelName(lm.GetCurLevelName());
        lm.ResetCurLevel();
    }

    public void SetCurCheckPoint(int _ind)
    {
        if (progressOnly)
        {
            if (_ind > curCheckPoint)
                curCheckPoint = _ind;
        }
        else
            curCheckPoint = _ind;

        if (saveProgressToDisc)
        {
            lm.SaveCurCheckPoint(curCheckPoint);
            lm.SaveCurLevelName(lm.GetCurLevelName());
        }
    }

    void OnApplicationQuit()
    {
        if (resetProgressOnQuit)
        {
            Debug.Log("Erasing Checkpoint progress for " + lm.GetCurLevelName());
            lm.ResetCurCheckPoint();
            lm.EraseSavedLevelName();
            lm.ResetCurLevel();
        }
        else
        {
            Debug.Log("Keeping Checkpoint progress for " + lm.GetCurLevelName());
            lm.SaveCurLevelName(lm.GetCurLevelName());
            lm.SaveCurCheckPoint(curCheckPoint);
        }
    }

    
}
