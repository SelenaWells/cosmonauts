﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour 
{

    private const string CHARKEY = "CharKey";
    private int curInd;

    [SerializeField]
    private CharacterSelectionUI charUI;
    [SerializeField]
    private Player player;

    private PlayerData pd;

    void Start()
    {
        pd = GameManager.instance.GetPlayerData();

        StartCoroutine(DelaySkinName());
    }

    public void PrevCharacter()
    {
        if (curInd > 0)
            curInd--;
        else
            curInd = player.GetCurSkinCount() - 1;

        SwitchCharacter();
    }

    public void NextCharacter()
    {
        if (curInd < player.GetCurSkinCount() - 1)
            curInd++;
        else
            curInd = 0;

        SwitchCharacter();
    }

    void SwitchCharacter()
    {
        player.SetPlayerSkin(curInd);
        charUI.SetCharName(player.GetCurSkin().name);
    }

    public void ConfirmCharacter()
    {
        pd.SetCurPlayerSkin(curInd);
    }

    IEnumerator DelaySkinName()
    {
        yield return new WaitForEndOfFrame();
        charUI.SetCharName(player.GetCurSkin().name);
        curInd = player.GetCurSkinInd();
    }
}
