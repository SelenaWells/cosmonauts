﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour 
{

    [SerializeField]
    private float speed;
    [SerializeField]
    private Transform leftPos;
    [SerializeField]
    private Transform rightPos;
    [SerializeField]
    private bool bounce;
    [SerializeField]
    private float bouncePower;
    [SerializeField]
    private LayerMask mask;


    private bool grounded;

    private Rigidbody2D rb;

    private bool pausePatrol;
	// Use this for initialization
	void Start () 
	{
        //
        SetYRotation(270);

        //get components
        rb = GetComponent<Rigidbody2D>();
    }
	
    void Update()
    {
        CheckGrounded();
    }

	// Update is called once per frame
	void FixedUpdate () 
	{
        if (pausePatrol)
            return;

        MoveEnemy();
        if (bounce)
        {
            BounceEnemy();
        }

    }

    void MoveEnemy()
    {
        //move enemy..forward
        transform.Translate(Vector3.forward * speed * Time.deltaTime);

        //rotate enemy based in waypoint x position
        if (transform.localPosition.x <= leftPos.localPosition.x)
        {
            SetYRotation(90);
        }
        else if (transform.localPosition.x >= rightPos.localPosition.x)
        {
            SetYRotation(270);
        }
    }

    void SetYRotation(float _yValue)
    {
        transform.localEulerAngles = new Vector3(0, _yValue, 0);
    }

    float GetYRotation()
    {
        return transform.localEulerAngles.y;
    }

    void CheckGrounded()
    {

        RaycastHit2D hit = Physics2D.Raycast((Vector2)transform.position + (Vector2.up * 0.1f),
            Vector2.down, Mathf.Infinity, mask);

        if (hit)
        {
            if (hit.distance < 0.11f)
                grounded = true;
            else
                grounded = false;
        }
        else
            grounded = false;
        
    }

    void BounceEnemy()
    {
        if (grounded)
        {
            rb.Sleep();
            rb.AddForce(Vector3.up * bouncePower, ForceMode2D.Impulse);
        }    
    }

    void OnCollisionEnter2D(Collision2D _col)
    {
        if (_col.collider.tag == "Enemy")
        {

            if (GetYRotation() > 90)
                SetYRotation(90);
            else
                SetYRotation(270);
        }
    }

    public void PausePatrol(bool _pause)
    {
        pausePatrol = _pause;
    }
}
