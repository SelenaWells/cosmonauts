﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuLevelManager : MonoBehaviour 
{

    [SerializeField]
    private bool displayAll = false;
    [SerializeField]
    private StartMenuLevel[] levels;

    private PlayerData pd;

	// Use this for initialization
	void Start () 
	{
        //getcomponents
        pd = GameManager.instance.GetPlayerData();

        if (!displayAll)
        {
            foreach (var level in levels)
            {
                if (pd.GetCurProgress() >= level.GetLevelInd())
                {
                    level.SetLevelPlayable(true);
                }
                else
                {
                    level.SetLevelPlayable(false);
                }

            }
        }
        else
        {
            foreach (var level in levels)
            {
                level.SetLevelPlayable(true);
            }
        }
	}
	
}
