﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{

    [SerializeField]
    private int maxHp = 5;

    private int curHp;

    // Use this for initialization
    void Start () 
	{
        curHp = maxHp;
	}

    public void AddHp(int _amount)
    {
        //only add health if not at max
        if (curHp < maxHp)
        {
            curHp += _amount;
            
        }

    }

    public void DamageHp(int _damage)
    {
        curHp -= _damage;

        //kill player if health at 0
        if (curHp <= 0)
        {
            curHp = 0;
            Die();
        }

        
    }

    void Die()
    {
        Destroy(this.gameObject);
    }

    public int GetCurHp()
    {
        return curHp;
    }

}
